namespace GoogleARCore.Examples.AugmentedImage {
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    
    public class RotatingGift : MonoBehaviour {
        // private bool StartRotation;
        public GameObject target;
        public Vector3 point = new Vector3(-0.02f, 0f, 0f);

        public int speed = 190;
        public int zRotation = 0;

        public Vector3 startingPosition = new Vector3();

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        void Awake() {
            startingPosition = this.gameObject.transform.position;
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        void Update() {
            // if (StartRotation == true) {
            //     gift.transform.RotateAround(target, point, 60 * Time.deltaTime);
            // }

            if (zRotation < 100) {
                this.gameObject.transform.RotateAround(target.gameObject.transform.position, point, speed * Time.deltaTime);
                this.gameObject.transform.eulerAngles = new Vector3(0f, 0f, 0f);
            } else if (zRotation > 100 && zRotation < 200) {
                this.gameObject.transform.position = startingPosition;
                if (zRotation > 130 && zRotation < 171) {
                    this.gameObject.transform.Rotate(0f, 6.75f, 0f, Space.World);
                }
            } else if (zRotation > 600) {
                zRotation = -15;
            }

            zRotation++;
        }
    }
}
