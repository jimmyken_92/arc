﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingGift : MonoBehaviour {
    // User Inputs
    public float degreesPerSecond = 15f; // 15.0f
    public float amplitude = 0.001f; // 0.001f
    public float frequency = 1f; // 1f
 
    // Position Storage Variables
    Vector3 posOffset = new Vector3();
    Vector3 tempPos = new Vector3();

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update () {
        posOffset = this.gameObject.transform.position;
        // Spin object around Y-Axis
        this.gameObject.transform.Rotate(new Vector3(0f, Time.deltaTime * degreesPerSecond, 0f), Space.World);

        // Float up/down with a Sin()
        tempPos = posOffset;
        tempPos.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;

        this.gameObject.transform.position = new Vector3(this.gameObject.transform.position.x, tempPos.y, this.gameObject.transform.position.z);
    }
}
