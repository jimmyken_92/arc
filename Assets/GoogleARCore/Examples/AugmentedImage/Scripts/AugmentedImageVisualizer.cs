﻿namespace GoogleARCore.Examples.AugmentedImage {   
    using System.Collections;
    using System.Collections.Generic;
    using System.Runtime.InteropServices;
    using GoogleARCore;
    using GoogleARCoreInternal;
    using UnityEngine;
    using UnityEngine.UI;

    public class AugmentedImageVisualizer : MonoBehaviour {
        // Augmented Image
        public AugmentedImage Image;
        // Model
        public GameObject ObjectsContainer;

        /// <summary>
        /// Update method of Unity
        /// </summary>
        // Update is called once per frame
        public void Update() {
            if (Image == null || Image.TrackingState != TrackingState.Tracking) {
                ObjectsContainer.SetActive(false);
                return;
            }
            // Get position
            // float halfWidth = Image.ExtentX / 2;
            // float halfHeight = Image.ExtentZ / 2;

            // Positioning object
            // ObjectsContainer.transform.localPosition = (halfWidth * Vector3.left) + (halfHeight * Vector3.back);
            ObjectsContainer.SetActive(true);
        }
    }
}
