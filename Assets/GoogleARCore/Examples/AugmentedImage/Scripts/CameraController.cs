﻿namespace GoogleARCore.Examples.AugmentedImage {
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class CameraController : MonoBehaviour {
        private Transform watchView;
        public Transform offsetView;
        private bool hit = false;
        public float transitionSpeed = 1f;

        /// <summary>
        /// Start is called on the frame when a script is enabled just before
        /// any of the Update methods is called the first time.
        /// </summary>
        void Start() {
            watchView = transform;
        }

        void GetObjectInfo() {
            if (Input.GetMouseButtonDown(0)) {
                RaycastHit hitInfo = new RaycastHit();
                hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);

                if (hit)
                    watchView = hitInfo.transform;
                else
                    watchView = offsetView;
            }
        }

        void Update() {
           GetObjectInfo();
        }

        /// <summary>
        /// LateUpdate is called every frame, if the Behaviour is enabled.
        /// It is called after all Update functions have been called.
        /// </summary>
        void LateUpdate() {
            transform.position = Vector3.Lerp(transform.position, new Vector3(watchView.position.x, watchView.position.y, watchView.position.z), Time.deltaTime * transitionSpeed);

            Vector3 currentAngle = new Vector3(
                Mathf.LerpAngle(transform.rotation.eulerAngles.x, watchView.transform.rotation.eulerAngles.x, Time.deltaTime * transitionSpeed),
                Mathf.LerpAngle(transform.rotation.eulerAngles.y, watchView.transform.rotation.eulerAngles.y, Time.deltaTime * transitionSpeed),
                Mathf.LerpAngle(transform.rotation.eulerAngles.z, watchView.transform.rotation.eulerAngles.z, Time.deltaTime * transitionSpeed)
            );

            transform.eulerAngles = currentAngle;
       }
    }
}
