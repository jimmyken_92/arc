﻿namespace GoogleARCore.Examples.AugmentedImage {
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class Flickering : MonoBehaviour
    {
        public GameObject _text;
        // Start is called before the first frame update
        void Start() {
            StartCoroutine(FlickeringEffect());
        }

        IEnumerator FlickeringEffect() {
            _text.SetActive(true);
            yield return new WaitForSeconds(Random.Range(0.5f, 2.5f));
            _text.SetActive(false);
            yield return new WaitForSeconds(Random.Range(0.01f, 0.05f));
            StartCoroutine(FlickeringEffect());
        }
    }
}
