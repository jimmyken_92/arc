﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RolexWatches_001_CameraManager_001 : MonoBehaviour
{
    private Transform watchView;
    public Transform startView;
    public float transitionSpeed;

    private float dollyAmount = 1f;

    void Start()
    { 
        watchView = transform;
    }

    void ClickObjectSelection()
    {
        if(Input.GetMouseButtonDown(0))
        {
            RaycastHit hitInfo = new RaycastHit();
            bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);

            if (hit)
            {
                // Debug.Log(hitInfo.transform.gameObject.name);
                watchView = hitInfo.transform;
            }
            else
            {
                watchView = startView;
            }
        }
    }

    private void Zoom()
    {
        if(Input.mouseScrollDelta.y > 0)
        {
            watchView.Translate(Vector3.forward * dollyAmount);
        }

        if (Input.mouseScrollDelta.y < 0)
        {
            watchView.Translate(Vector3.forward * -dollyAmount);
        }
    }

    private void Update()
    {
        ClickObjectSelection();
        // Zoom();
    }

    void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, watchView.position, Time.deltaTime * transitionSpeed);

        Vector3 currentAngle = new Vector3(
            Mathf.LerpAngle(transform.rotation.eulerAngles.x, watchView.transform.rotation.eulerAngles.x, Time.deltaTime * transitionSpeed),
            Mathf.LerpAngle(transform.rotation.eulerAngles.y, watchView.transform.rotation.eulerAngles.y, Time.deltaTime * transitionSpeed),
            Mathf.LerpAngle(transform.rotation.eulerAngles.z, watchView.transform.rotation.eulerAngles.z, Time.deltaTime * transitionSpeed));

        transform.eulerAngles = currentAngle;
    }
}
