# ARC

## Updates

### Posted by [@jimmyken_92](h):
* Added sandbox for testing (Tests scene)
* Steps completed:
    * ~~Track image~~
    * ~~AR object appears~~
    * ~~Child objects appears~~
    * ~~Child objects turn around AR object (script animation)~~
    * ~~Child object gifts appears~~
    * ~~Child object gifts moves up (script animation)~~
* Steps to modify:
    * Child objects turn around AR object (embedded animation)
    * Child object gifts moves up (embedded animation)
* Working on:
    * Open child object gift as an object no script animation
    * Recognise hand branch:
        * `Recognise hand`
        * Child object watch appears
        * Put watch in detected hand
    * Object appears branch:
        * Child object watch appears
        * Customer puts his hand in the watch position
    * `Share on Social Media`

